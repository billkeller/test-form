import React from 'react';
import { withFormsy } from 'formsy-react';

class RadioGroup extends React.Component {
  
  changeValue = (item) => {
    this.props.setValue(item);
  }

  render() {
    const { items, name } = this.props;
    return (
      <div key="">
        {items.map((item, i) => {
          return (
          <label key={i}>
            <input
              type="radio"
              name={name}
              onChange={() => this.changeValue(item)}
            />
            {item}
          </label>
        )})}
      </div>
    )
  }
}

export default withFormsy(RadioGroup);