import { withFormsy } from 'formsy-react';
import React from 'react';

class TextInput extends React.Component {
  
  changeValue = (event) => {
    this.props.setValue(event.currentTarget.value);
  }

  render() {
    return (
      <label>Name:<br />
        <input
          onChange={this.changeValue}
          type="text"
          value={this.props.getValue() || ''}
        />
      </label>
    )
  }
}
export default withFormsy(TextInput);