import React from 'react';
import Formsy from 'formsy-react';
import TextInput from './components/TextInput';
import RadioGroup from './components/RadioGroup';
import './App.css';

export default class App extends React.Component {
  submit(model) {
    console.log('model: ', model);
  }

  render() {
    return (
      <div className="App">
        <section style={{padding: '100px'}}>
          <Formsy onValidSubmit={this.submit}>
            <p>On submit, this form will log Formsy's model to the console.</p>
            <TextInput
              name="fname"
            />
            <p>Preferred Cheese:</p>
            <RadioGroup
              name="favorite-cheese"
              items = {["cheddar", "swiss"]}
            />
            <button type="submit">Submit</button>
          </Formsy>
        </section>
      </div>
    );
  }
}
